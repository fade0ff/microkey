/** Synchronous Serial Library - Configuration
	Communication via the SSP peripheral (i.e. SPI)
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "spi.h"
#include "pin.h"

/* NOTE! This is just a demo - there is currently no SPI peripheral in this project */

/** Assign physical SSP devices to virtual devices - no need to use them all */
const spi_config_t spi_device_cfg[SPI_VIRTUAL_DEVICE_NUM] = {
	/* device0 */
	{ LPC_SPI0 },
	/* device1 */
	/*{ LPC_SPI1 },*/
};


/** Channel configuration for SSD1331 OLED controller. */

#define SPI_CFG_IO    (SPI_CFG_ENABLE | SPI_CFG_MASTER | SPI_CFG_MSB_FIRST | SPI_CFG_CPHA0 | SPI_CFG_CPOL_LO | SPI_CFG_SPOL0_LO | SPI_CFG_SPOL1_LO | SPI_CFG_SPOL2_LO | SPI_CFG_SPOL3_LO)

#define SPI_CTRL_IO   ((SPI_TXCTL_SSEL0_ASSERT | SPI_TXCTL_SSEL1_DEASSERT | SPI_TXCTL_SSEL2_DEASSERT | SPI_TXCTL_SSEL3_DEASSERT | SPI_TXCTL_LEN(8))>>16)

#define SPI_DLY_DEFAULT (SPI_DLY_PRE_DELAY(2) | SPI_DLY_POST_DELAY(2) | SPI_DLY_FRAME_DELAY(2) | SPI_DLY_TRANSFER_DELAY(2))

/** Communication channel configuration  */
const spi_channel_cfg_t spi_channel_cfg[SPI_CHANNEL_CONFIG_NUM] = {
			 /* device id     frequency   config,          tx control,     delay            chip select pin */
	/* ch0 */ { 0 /* dev0 */, 6000000,    SPI_CFG_IO,      SPI_CTRL_IO,    SPI_DLY_DEFAULT, SPI_CS_HW        },
};
