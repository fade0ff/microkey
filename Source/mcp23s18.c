/** MCP23S18 mini driver
	Driver for MCP23S18 16bit I/O expander
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2016 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "mcp23s18.h"
#include "spi.h"
#include "prc.h"

#define MCP23S18_ACTIVE 0x80
#define MCP23S18_MASK   0x3F

static u8 mcp23s18_write_tx_buf[3];
static u8 mcp23s18_write_rx_buf[3];
static u8 mcp23s18_read_tx_buf[3];
static u8 mcp23s18_read_rx_buf[3];

static u8 mcp23s18_state_write;
static u8 mcp23s18_state_read;

/** Callback function for TMP102 I2C read transfer.
 */
static void MCP23S18_CallbackRead(void) {
	mcp23s18_state_read = 0;
}

/** Callback function for TMP102 I2C write transfer.
 */
static void MCP23S18_CallbackWrite(void) {
	mcp23s18_state_write = 0;
}


/** Start write transfer
	@return SPI Status (see SPI_TRANSFER_xxx)
 */
u8 MCP23S18_Write(u8 reg, u8 value) {
	while (SPI_GetChPendingCnt(SPI_CH_MCP23S18));
	mcp23s18_write_tx_buf[0] = MCP23S18_WRITE_COMMAND;
	mcp23s18_write_tx_buf[1] = reg;
	mcp23s18_write_tx_buf[2] = value;
	mcp23s18_state_write = reg | MCP23S18_ACTIVE;
	return SPI_StartTransferX(SPI_CH_MCP23S18, mcp23s18_write_tx_buf, mcp23s18_write_rx_buf, 3, 1, 0, SPI_CS_RELEASE, NULL, MCP23S18_CallbackWrite);
}

/** Start read transfer
	@return I2C SPI Status (see SPI_TRANSFER_xxx)
 */
u8 MCP23S18_StartRead(u8 reg) {
	while (SPI_GetChPendingCnt(SPI_CH_MCP23S18));
	mcp23s18_read_tx_buf[0] = MCP23S18_READ_COMMAND;
	mcp23s18_read_tx_buf[1] = reg;
	mcp23s18_read_tx_buf[2] = 0; /* dummy */
	mcp23s18_state_read = reg | MCP23S18_ACTIVE;
	return SPI_StartTransferX(SPI_CH_MCP23S18, mcp23s18_read_tx_buf, mcp23s18_read_rx_buf, 3, 1, 1, SPI_CS_RELEASE, NULL, MCP23S18_CallbackRead);
}

/** Read received byte
	@return last received byte
 */
u8  MCP23S18_GetRxByte(void) {
	return mcp23s18_read_rx_buf[2];
}

void MCP23S18_WaitForCompletion(void) {
	while (SPI_GetChPendingCnt(SPI_CH_MCP23S18));
}
