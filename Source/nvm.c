/** NVM library
	Non volatile memory simulation implemented via flash
	LPC8xx ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "iap.h"
#include "nvm.h"
#include "prc.h"
#include "systime.h"
#include <string.h>
#include <stdlib.h>

static const u32 nvm_sector_start[2] = {NVM_SECTOR_0, NVM_SECTOR_1};


static u8 nvm_active_sector; ///! active NVM sector (0 or 1)
static u8 nvm_active_page;   ///! active NVM page (0..15)
static u8 nvm_valid;         ///! valid page found


/** NVRAM initialization */
void NVM_Init(void) {
	u32 test;
	u32 page;

	// first dword in 1st page should be a magic pattern to mark this sector as active
	test = ((u32*)nvm_sector_start[nvm_active_sector])[0];
	if (test != NVM_MAGIC_PATTERN) {
		nvm_active_sector++;
		test = ((u32*)nvm_sector_start[nvm_active_sector])[0];
	}

	if (test == NVM_MAGIC_PATTERN) {
		// search active page
		nvm_active_page = 0;
		for (page=NVM_SECTOR_SIZE/NVM_PAGE_SIZE-1; page >0; page--) { // skip page one since pattern was found already
			test = ((u32*)nvm_sector_start[nvm_active_sector])[page*(NVM_PAGE_SIZE/4)];
			if (test == NVM_MAGIC_PATTERN) {
				nvm_active_page = page;
				break;
			}
		}
		nvm_valid = 1;
	} else {
		nvm_active_sector = 0;
		nvm_active_page = 0;
		nvm_valid = 0;
	}
}

/** Wait for the current NVRAM operation to be completed (poll I2C EEPROM acknowledge) */
void NVM_WaitForCompletion(void) {
	// dummy
}

void NVM_EraseSector(u8 sector) {
	PRC_Int_Disable();
	u32 dummy_offset, dummy_content;
	u8 status = iap_blank_check_sectors(sector, sector, &dummy_offset, &dummy_content);
	if (status != IAP_STAT_CMD_SUCCESS) {
		iap_prepare_sectors(sector, sector);
		iap_erase_sectors(sector, sector);
	}
	PRC_Int_Enable();
}

/** Write data at a given address.
	@param adr    start address  in NVRAM
	@param data   pointer to buffer which contains data to be written
	@param len    number of bytes to be written
	@return success
 */
u8 NVM_Write(u8 adr, u8 *data, u8 len) {
	u8 status;
	u8 buffer[NVM_PAGE_SIZE];
	u8 sector;
	u32 address;
	u32 delete_sector = 0xffffffff;

	if (len > NVM_PAGE_SIZE-4)
		return 0;

	if (nvm_valid==0) {
		// delete sector
		address = nvm_sector_start[nvm_active_sector] + (nvm_active_page*NVM_PAGE_SIZE);
		sector =  IAP_GET_SECTOR(address);
		NVM_EraseSector(sector);
	} else if (++nvm_active_page >= NVM_SECTOR_SIZE/NVM_PAGE_SIZE) {
		// swap sectors
		delete_sector = IAP_GET_SECTOR(nvm_sector_start[nvm_active_sector]);
		nvm_active_sector = !nvm_active_sector;
		nvm_active_page = 0;
		address = nvm_sector_start[nvm_active_sector];
		sector =  IAP_GET_SECTOR(address);
		NVM_EraseSector(sector);
	}

	address = nvm_sector_start[nvm_active_sector] + (nvm_active_page*NVM_PAGE_SIZE);
	sector =  IAP_GET_SECTOR(address);

	((u32*)buffer)[0] = NVM_MAGIC_PATTERN;
	memcpy(&buffer[4],data,len);

	PRC_Int_Disable();
	iap_prepare_sectors(sector, sector);
	// write magic pattern
	status = iap_copy_to_flash((u32)buffer, address, NVM_PAGE_SIZE);
	// delete other sector if we just swapped successfully
	if (delete_sector != 0xffffffff && status == IAP_STAT_CMD_SUCCESS)
		NVM_EraseSector(delete_sector);
	PRC_Int_Enable();

	return status == IAP_STAT_CMD_SUCCESS;
}

/** Read data from a given address
	@param adr    start address in NVRAM
	@param data   pointer to buffer where read data is stored
	@param len    number of bytes to be read
	@return success
 */
u8 NVM_Read(u8 adr, u8 *data, u8 len) {
	u32 address;
	if (nvm_valid == 0)
		return 0;

	address = nvm_sector_start[nvm_active_sector] + (nvm_active_page*NVM_PAGE_SIZE);
	memcpy(data,(u8*)(address+4+adr), len);
	return 1;
}
