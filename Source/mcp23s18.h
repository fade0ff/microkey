/** MCP23S18 mini driver
	Driver for MCP23S18 16bit I/O expander
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2016 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef MCP23S18_H
#define MCP23S18_H

#define MCP23S18_READ_COMMAND  0x41
#define MCP23S18_WRITE_COMMAND 0x40

/* Register defintion for Bank0 mode (default) */
#define MCP23S18_IODIRA   0x00 /**< direction IO7..IO0 - 1: input, 0: output */
#define MCP23S18_IODIRB   0x01 /**< direction IO7..IO0 - 1: input, 0: output*/
#define MCP23S18_IPOLA    0x02 /**< polarity inversion IP7..IP0 - 1: inverted, 0: not inverted */
#define MCP23S18_IPOLB    0x03 /**< polarity inversion IP7..IP0 - 1: inverted, 0: not inverted */
#define MCP23S18_GPINTENA 0x04 /**< interrupt on change GPINT7..GPINT0 - 1: enable, 0: disable */
#define MCP23S18_GPINTENB 0x05 /**< interrupt on change GPINT7..GPINT0 - 1: enable, 0: disable */
#define MCP23S18_DEFVALA  0x06 /**< default value DEF7..DEF0 */
#define MCP23S18_DEFVALB  0x07 /**< default value DEF7..DEF0 */
#define MCP23S18_INTCONA  0x08 /**< interrupt config IOC7..IOC0 - 1: compare with DEFVAL, 0: compare with previous state */
#define MCP23S18_INTCONB  0x09 /**< interrupt config IOC7..IOC0 - 1: compare with DEFVAL, 0: compare with previous state */
#define MCP23S18_IOCON    0x0A /**< Configuration register - BANK MIRROR SEQOP - - ODR INTPOL INTCC */
#define MCP23S18_IOCON_   0x0B /**< same as IOCON */
#define MCP23S18_GPPUA    0x0C /**< Pullup configuration PU7..PU0 - 1: enable, 0: disable */
#define MCP23S18_GPPUB    0x0D /**< Pullup configuration PU7..PU0 - 1: enable, 0: disable */
#define MCP23S18_INTFA    0x0E /**< Interrupt flag INT7..INTO - 1: pin caused interrupt, 0: pin didn't cause interrupt (read only) */
#define MCP23S18_INTFB    0x0F /**< Interrupt flag INT7..INTO - 1: pin caused interrupt, 0: pin didn't cause interrupt (read only)  */
#define MCP23S18_INTCAPA  0x10 /**< Input capture ICP7..ICP0 - port pin levels captured at interrupt */
#define MCP23S18_INTCAPB  0x11 /**< Input capture ICP7..ICP0 - port pin levels captured at interrupt */
#define MCP23S18_GPIOA    0x12 /**< GPIO in/out register GP7..GP0 - logic level of port pins */
#define MCP23S18_GPIOB    0x13 /**< GPIO in/out register GP7..GP0 - logic level of port pins */
#define MCP23S18_OLATA    0x14 /**< Ouput latch OL7..OL0 - logic level of output latch */
#define MCP23S18_OLATB    0x15 /**< Ouput latch OL7..OL0 - logic level of output latch */

#define MCP23S18_IOCON_BANK_0       (0U << 7) /**< bank 0 mode */
#define MCP23S18_IOCON_BANK_1       (1U << 7) /**< bank 1 mode */
#define MCP23S18_IOCON_MIRROR_OFF   (0U << 6) /**< separate INT pins for both ports */
#define MCP23S18_IOCON_MIRROR_ON    (1U << 6) /**< ORed INT pins for both ports */
#define MCP23S18_IOCON_SEQOP_EN     (0U << 5) /**< enable sequential operation (increase addres pointer) */
#define MCP23S18_IOCON_SEQOP_DIS    (1U << 5) /**< disable sequential operation (byte mode) */
#define MCP23S18_IOCON_ODR_DIS      (0U << 2) /**< disable open drain mode for interrupt pins */
#define MCP23S18_IOCON_ODR_EN       (1U << 2) /**< enable open drain mode for interrupt pins */
#define MCP23S18_IOCON_INTPOL_HI    (1U << 1) /**< polarity of interrupt pins is high active */
#define MCP23S18_IOCON_INTPOL_LO    (0U << 1) /**< polarity of interrupt pins is low active */
#define MCP23S18_IOCON_INTCC_GPIO   (0U << 0) /**< reading GPIO register clears the interrupt */
#define MCP23S18_IOCON_INTCC_INTCAP (1U << 0) /**< reading INTCAP register clears the interrupt */



extern u8   MCP23S18_Write(u8 reg, u8 value);
extern u8   MCP23S18_StartRead(u8 reg);
extern void MCP23S18_WaitForCompletion(void);
extern u8   MCP23S18_GetRxByte(void);

#endif

