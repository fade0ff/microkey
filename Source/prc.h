/** Processor library
	Provide abstraction on CPU level
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef PRC_H
#define PRC_H

#include "global.h"


#define MU0 0x55555555UL  // MU0 == ((-1UL)/3UL) == ...01010101_2
#define MU1 0x33333333UL  // MU1 == ((-1UL)/5UL)   == ...00110011_2
#define MU2 0x0f0f0f0fUL  // MU2 == ((-1UL)/17UL)  == ...00001111_2
#define MU3 0x00ff00ffUL  // MU3 == ((-1UL)/257UL)  == (8 ones)
#define MU4 0x0000ffffUL  // MU4 == ((-1UL)/65537UL) == (16 ones)

static inline u8 ld_neq(u32 x, u32 y) {
	// Return whether floor(log2(x))!=floor(log2(y))
	return (x^y) > (x&y);
}

static inline u32 PRC_GetMSB(u32 x) {
	// Return index of highest bit set.
	// Return 0 if no bit is set.
	if (x==0)
		return 0;
	u32 r = (u32)ld_neq(x, x & MU0)
        + ((u32)ld_neq(x, x & MU1) << 1)
        + ((u32)ld_neq(x, x & MU2) << 2)
        + ((u32)ld_neq(x, x & MU3) << 3)
        + ((u32)ld_neq(x, x & MU4) << 4);
    return r+1;
}
#undef MU0
#undef MU1
#undef MU2
#undef MU3
#undef MU4


extern volatile int prc_irq_nesting_lvl;

/** Nested interrupt enable */
#define PRC_Int_Enable()  { if (--prc_irq_nesting_lvl == 0) __ASM volatile ("cpsie i"); }
/** Nested interrupt disable */
#define PRC_Int_Disable() { if (prc_irq_nesting_lvl++ == 0) __ASM volatile ("cpsid i"); }


/** Scale an u32 value with a 15bit percentage (0x8000 == 100%)
    @param v32 value to scale
    @param pct15 the percentage value [0..0x8000]
    @return the scaled value
 */
static __INLINE u32 PRC_Scale_u32_u15(u32 v32, u16 pct15) {
	return (((u32)(v32)>>15)*(u32)(pct15)) + ((((u32)(v32)&0x7fff)*(u32)(pct15))/0x8000);
}

/** Reverse byte order of 32bit number.
	0x12345678 -> 0x78563412
	@param val 32bit value to reserve.
	@return value in reversed byte order
*/
static __INLINE u32 PRC_ReverseByteOrder(u32 val) {
	__ASM volatile("rev %0, %1" : "=r" (val) : "r" (val));
	return val;
}

/** Reverse byte order of 32bit number per 16bit.
	0x12345678 -> 0x34127856
	@param val 32bit value to reserve.
	@return value in reversed byte order
*/
static __INLINE u32 PRC_ReverseByteOrderW(u32 val) {
	__ASM volatile("rev16 %0, %1" : "=r" (val) : "r" (val));
	return val;
}

static __INLINE u32 PRC_Locked_Bitmask(u32 *ptr, u32 nand, u32 or) {
	u32 val;
	PRC_Int_Disable();
	val = (*ptr & (u32)~nand) | or;
	*ptr = val;
	PRC_Int_Enable();
	return val;
}

static __INLINE u32 PRC_Locked_Or(u32 *ptr, u32 or) {
	u32 val;
	PRC_Int_Disable();
	val = *ptr | or;
	*ptr = val;
	PRC_Int_Enable();
	return val;
}

static __INLINE u32 PRC_Locked_And(u32 *ptr, u32 and) {
	u32 val;
	PRC_Int_Disable();
	val = *ptr & and;
	*ptr = val;
	PRC_Int_Enable();
	return val;
}

static __INLINE u32 PRC_Locked_ReadClear(volatile u32 *data, u32 bm) {
	u32 retval;
	PRC_Int_Enable();
	retval = *data & bm;
	*data &= (u32)~bm;
	PRC_Int_Disable();
	return retval;
}

/** Calculate percentage of a 32bit value v32 where percentage pct15 is given in 100% == 0x8000
	Note: (0xffffffff>>15)*0x8000 just fits into 32bit -> no overflow as long as pct15 is <= 0x8000
	@param v32  unsigned 32bit value
	@param pct15 percentage in PCT15 resolution [0..0x8000], where 100% == 0x8000
	@result percentage of value (v32*pct15/0x8000)
*/
#define u32_pct15(v32, pct15) ( (((u32)(v32)>>15)*(u32)(pct15)) + ((((u32)(v32)&0x7fff)*(u32)(pct15))/0x8000) )

#endif

