/** MicroKey
	A kernal switcher an more for the 250466+ C64 mainboard
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2021 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "priowrap.h"
#include "dma.h"
#include "crc.h"
#include "nvm.h"
#include "systick.h"
#include "systime.h"
#include "mrt.h"
#include "spi.h"
#include "mcp23s18.h"
#include "system_LPC8xx.h"
#include <string.h>
#include <stdlib.h>


#define DEBUG_PU    0   // enable pullups for debugging
#define DEBUG_NVRAM 0   // no NVRAM writes

#define RESTORE_CHECK_KEY_MS   500 ///< Time in ms that Restore needs to be pressed to enter key check mode
#define RESTORE_RESET_MS      3000 ///< Time in ms that Restore needs to be pressed to enter reset mode
#define RESTORE_HARD_RESET_MS 5000 ///< Time in ms that Restore needs to be pressed to enter hard reset mode

#define RESET_DELAY_MS         200 ///< Time in ms to pull the reset line low for a reset
#define RESET_EXROM_MS         280 ///< Time to in ms to keep the EXPOM line pulled low after releasing the reset line for hard reset

#define BLINK_ON_MS            200 ///< ON time in ms when blinking codes
#define BLINK_OFF_MS           300 ///< OFF time in ms when blinking codes


#define CONFIG_DELAY_MS       5000 ///< Minimum delay between writing configuration to NVRAM

#define IO_PORT_B_NOLED_MASK   0x3f //< Mask used to make out the LED bits
#define IO_PORT_B_KERNAL_MASK  0x0e //< Mask used to make out the kernal bits


#define PIN_DEBOUNCE_COUNT      3   //< Debounce count for pins
#define IO_DEBOUNCE_COUNT       3   //< Debounce count for reading PortB through the IO expansion
#define PIN_IRQ_STUCK_COUNT     3   //< Stuck count for the PA0 edge detection
#define PA0_IRQ_DELAY			3   //< Number of 1ms interrupts to pass between PA0 edge and start of own keyscan

/* Output pins on I/O Expander Port B */
#define IO_JOYSWAP      (1U<<0)
#define IO_KERNAL_A15   (1U<<1)
#define IO_KERNAL_A14   (1U<<2)
#define IO_KERNAL_A13   (1U<<3)
#define IO_SID_A5_SEL   (1U<<4)
#define IO_SID_A8_SEL   (1U<<5)
#define IO_LED1         (1U<<6)
#define IO_LED2         (1U<<7)

// C64 PortB key definition (with PortA0 selected)
#define KEY_DOWN        (1U<<7)
#define KEY_F5          (1U<<6)
#define KEY_F3          (1U<<5)
#define KEY_F1          (1U<<4)
#define KEY_F7          (1U<<3)
#define KEY_RIGHT       (1U<<2)
#define KEY_RETURN      (1U<<1)
#define KEY_DEL         (1U<<0)


typedef enum {
	IDLE,
	CHECK_KEYS,
	BLINK_KERNAL,
	BLINK_SID,
	BLINK_JOYSWAP,
	RESET,
	HARD_RESET
} system_state_t;

// variables
u32 ms_counter;
u8 s_counter;
u16 restore_counter;
u16 restore_debounce_counter;
u16 pa0_counter;
u32 pa0_edge_counter;
u16 pa0_irq_stuck_counter;
u16 function_counter;
u16 blink_counter;
u16 config_delay_counter;
u16 reset_counter;
u16 hard_reset_counter;

system_state_t system_state;
system_state_t state_after_blink_kernal;
system_state_t state_after_blink_sid;

/// I/O Port B outputs
typedef union {
	u8 byte;
	struct {
		u8 joyswap:1;
		u8 kernal_a15:1;
		u8 kernal_a14:1;
		u8 kernal_a13:1;
		u8 sid_a5_sel:1;
		u8 sid_a8_sel:1;
		u8 led2:1;
		u8 led1:1;
	} bit;
} io_port_b_t;

typedef struct {
	io_port_b_t io_port_b;
} config_t;


io_port_b_t io_port_b, io_port_b_old;
u8 cia_port_b[IO_DEBOUNCE_COUNT];

/// NVRAM configuration structure
config_t config = {
	.io_port_b.byte = 1 // all kernal selects=0, all SID selects=0, joyswwap off (high)
};

config_t config_old;

u16      nvram_write;
#if DEBUG_NVRAM
config_t config_nvram;
#endif

/** debounce structure for the input pins */
typedef struct {
	u8 pin;          ///! pin number
	s8 ctr;          ///! debounce counter
	u8 state;        ///! debounce state: 0: debounced, 1: debouncing ongoing
	volatile u8 lvl; ///! debounced logic level
	u8 inact_lvl;    ///! inactive level: 0: low, 1: high
} pin_debounce_t;


pin_debounce_t pins[] = {
	{
		.pin = PIN_RESTORE,
		.ctr = 0,
		.state = 0,
		.lvl = 0,
		.inact_lvl = 1
	}
};

u8 pin_debounce(u8 ch) {
	u8 changed = 0;
	pin_debounce_t *p = &pins[ch];
	u8 lvl = PIN_ChGet(p->pin) ^ p->inact_lvl;
	if (p->state == 0) {
		// not yet debouncing
		if (lvl != p->lvl) {
			// state change -> start debouncing
			p->ctr = PIN_DEBOUNCE_COUNT;
			p->state = 1;
		}
	} else {
		// already debouncing
		if (lvl != p->lvl) {
			// current level is different than the debounced level
			if (--p->ctr <= 0) {
				// new level was debounced
				p->state = 0;
				p->lvl = lvl;
				changed = 1;
			}
		} else {
			// current level is the same as the debounced level
			if (++p->ctr >= PIN_DEBOUNCE_COUNT) {
				// reset debounce state
				p->state = 0;
			}
		}
	}
	return changed;
}

u8 pin_state(u8 ch) {
	return pins[ch].lvl;
}

static __INLINE u8 PRC_BitmaskU8(u8 *ptr, u8 nand, u8 or) {
	u8 val;
	val = (*ptr & (u8)~nand) | or;
	*ptr = val;
	return val;
}

void set_ext_pins(void) {
	if (io_port_b.byte != io_port_b_old.byte) {
		MCP23S18_Write(MCP23S18_GPIOB, io_port_b.byte);
		io_port_b_old.byte = io_port_b.byte;
	}
}

void set_leds_idle(void) {
	// leds are low active - if joystick is not swapped, LED1 is on, else LED2 is on
	io_port_b.bit.led1 = !config.io_port_b.bit.joyswap;
	io_port_b.bit.led2 = config.io_port_b.bit.joyswap;
}

void set_leds_off(void) {
	// leds are low activ - if joystick is not swapped, LED1 is on, else LED2 is on
	io_port_b.bit.led1 = 1;
	io_port_b.bit.led2 = 1;
}


u16 calc_crc(u8 *data, u16 len) {
	int i;
	CRC_SetMode(CRC_MODE_POLY_CCITT);
	CRC_SetSeed(0xFFFF);
	for (i=0; i<len; i++)
		CRC_SetData8(data[i]);
	return CRC_GetSum();
}

u8 write_config(config_t *c) {
	nvram_write++;
#if DEBUG_NVRAM
	config_nvram.io_port_b.byte = config.io_port_b.byte;
	return 1;
#else
	u8 buffer[sizeof(config_t)+2];
	// write to NVRAM
	memcpy(&buffer[0],(u8*)c, sizeof(config_t));
	u16 crc = calc_crc((u8*)c, sizeof(config_t));
	buffer[sizeof(config_t)] = (u8)(crc & 0xff);
	buffer[sizeof(config_t)+1] = (u8)(crc >> 8);
	return NVM_Write(0, &buffer[0], sizeof(config_t)+2);
#endif
}

u8 read_config(config_t *c) {
#if DEBUG_NVRAM
	c->io_port_b.byte = config_nvram.io_port_b.byte;
	return 1;
#else
	u8 buffer[sizeof(config_t)+2];
	// read config from NVRAM
	if (NVM_Read(0, buffer, sizeof(config_t)+2) != 0) {
		// check crc
		u16 crc = calc_crc((u8*)&buffer, sizeof(config_t));
		u16 crc_nvm = buffer[sizeof(config_t)] | (buffer[sizeof(config_t)+1]<<8);
		if (crc == crc_nvm) {
			memcpy((u8*)c, &buffer[0], sizeof(config_t));
			return 1;
		}
	}
	return 0;
#endif
}

void task_1ms(u32 dummy) {
	(void)dummy;

	if (pa0_counter>0)
		pa0_counter--;
	else {
		if (pa0_irq_stuck_counter >= PIN_IRQ_STUCK_COUNT) {
			// reset stuck counter and enable edge interrupt again
			pa0_irq_stuck_counter = 0;
			PIN_ClrIntStatus(PIN_INT(INT_PA0));
			PIN_EnableRisingEdgeInt(PIN_INT(INT_PA0),1);
		}
	}

	if (config_delay_counter>0)
		config_delay_counter--;

	int restore_counter_old = restore_counter;
	pin_debounce(0);       // only Restore pin debounced
	if (pin_state(0)==1) { // pin logic is low active, but debounced state is logical level
		if (restore_counter < 0xffff)
			restore_counter++;
	} else
		restore_counter = 0;

	// check if Restore was pressed so long that a hard reset should be performed
	if (restore_counter >= RESTORE_HARD_RESET_MS) {
		if (system_state != HARD_RESET) {
			system_state = HARD_RESET;
			function_counter = 0;
		}
	} else 	if (restore_counter == 0) {
		// Restore was released
		if ((config_old.io_port_b.byte != config.io_port_b.byte) &&  (config_delay_counter==0)) {
			// write config to NVRAM
			write_config(&config);
			config_old.io_port_b.byte = config.io_port_b.byte;
			config_delay_counter = CONFIG_DELAY_MS;
		}
		// don't leave blinking states automatically
		if (system_state == CHECK_KEYS) {
			// Check reset
			if (restore_counter_old >= RESTORE_RESET_MS) {
				// Pressed long enough to cause a reset
				system_state = RESET;
				function_counter = 0;
			} else {
				// check if there was a kernal change, if so: reset, else return to idle
				if (((io_port_b.byte ^ config.io_port_b.byte) & IO_PORT_B_KERNAL_MASK) != 0 ) {
					system_state = RESET;
					function_counter = 0;
				} else {
					io_port_b.byte =  config.io_port_b.byte & IO_PORT_B_NOLED_MASK;
					set_leds_idle();
					system_state = IDLE;
				}
			}
		}
	}

	// handle system states
	switch (system_state) {
		case IDLE:
			if (restore_counter > RESTORE_CHECK_KEY_MS) {
				// switch off LED(s) to mark that we are waiting for a key
				set_leds_off();
				read_config(&config_old); // config_old contains config from NVRAM
				system_state = CHECK_KEYS;
				function_counter = 0;
			} else {
				io_port_b.byte =  config.io_port_b.byte & IO_PORT_B_NOLED_MASK;
				set_leds_idle();
			}
			break;
		case RESET:
			if (function_counter == 0) {
				reset_counter++;
				PIN_ChSet(PIN_INTRST, 0); // pull reset line low
				io_port_b.byte =  config.io_port_b.byte & IO_PORT_B_NOLED_MASK;
				set_leds_idle();
			} else {
				if (function_counter >= RESET_DELAY_MS) {
					PIN_ChSet(PIN_INTRST, 1); // release reset line
					system_state = IDLE;
					restore_counter = 1;
				}
			}
			function_counter++;
			break;
		case HARD_RESET:
			if (function_counter == 0) {
				hard_reset_counter++;
				PIN_ChSet(PIN_INTRST, 0); // pull reset line low
				PIN_ChSet(PIN_EXROM, 0);  // pull exrom line low
				io_port_b.byte =  config.io_port_b.byte & IO_PORT_B_NOLED_MASK;
				set_leds_idle();
			} else {
				if (function_counter >= RESET_DELAY_MS+RESET_EXROM_MS) {
					PIN_ChSet(PIN_EXROM, 1); // release exrom line
					system_state = IDLE;
					restore_counter = 1;
				}
				if (function_counter >= RESET_DELAY_MS) {
					PIN_ChSet(PIN_INTRST, 1); // release reset line
				}
			}
			function_counter++;
			break;
		case BLINK_JOYSWAP:
			switch (function_counter) {
				case 0:
					// leds are low activ - if joystick is not swapped, LED1 is on, else LED2 is on
					io_port_b.bit.led1 = config.io_port_b.bit.joyswap;
					io_port_b.bit.led2 = !config.io_port_b.bit.joyswap;
					break;
				case BLINK_ON_MS:
					io_port_b.bit.led1 = !config.io_port_b.bit.joyswap;
					io_port_b.bit.led2 = config.io_port_b.bit.joyswap;
					break;
				case BLINK_ON_MS+BLINK_OFF_MS:
					// leds are low activ - if joystick is not swapped, LED1 is on, else LED2 is on
					io_port_b.bit.led1 = config.io_port_b.bit.joyswap;
					io_port_b.bit.led2 = !config.io_port_b.bit.joyswap;
					break;
				case BLINK_ON_MS+BLINK_OFF_MS+BLINK_ON_MS:
					io_port_b.bit.led1 = !config.io_port_b.bit.joyswap;
					io_port_b.bit.led2 = config.io_port_b.bit.joyswap;
					break;
				case BLINK_ON_MS+BLINK_OFF_MS+BLINK_ON_MS+BLINK_OFF_MS:
					// switch off
					set_leds_off();
					break;
				case BLINK_ON_MS+BLINK_OFF_MS+BLINK_ON_MS+BLINK_OFF_MS+BLINK_ON_MS:
					function_counter = 0;
					restore_counter = RESTORE_CHECK_KEY_MS;
					system_state = CHECK_KEYS;
					break;
			}
			if (system_state==BLINK_JOYSWAP)
				function_counter++;
			break;
		case BLINK_SID:
			switch (function_counter) {
				case 0:
					blink_counter = 2*(1+
						config.io_port_b.bit.sid_a8_sel*2+
						config.io_port_b.bit.sid_a5_sel);
					io_port_b.bit.led2 = 0; // switch on
					break;
				case BLINK_ON_MS:
					io_port_b.bit.led2 = 1; // switch off
					blink_counter--;
					break;
				case BLINK_ON_MS+BLINK_OFF_MS:
					blink_counter--;
					if (blink_counter==0) {
						restore_counter = RESTORE_CHECK_KEY_MS;
						system_state = state_after_blink_sid;
						function_counter = 0;
					} else {
						// switch on
						io_port_b.bit.led2 = 0;
						function_counter = 1;
					}
					break;
			}
			if (system_state==BLINK_SID)
				function_counter++;
			break;
		case BLINK_KERNAL:
			switch (function_counter) {
				case 0:
					blink_counter = 2*(1+
						config.io_port_b.bit.kernal_a15*4+
						config.io_port_b.bit.kernal_a14*2+
						config.io_port_b.bit.kernal_a13);
					io_port_b.bit.led1 = 0; // switch on
					break;
				case BLINK_ON_MS:
					io_port_b.bit.led1 = 1; // switch off
					blink_counter--;
					break;
				case BLINK_ON_MS+BLINK_OFF_MS:
					blink_counter--;
					if (blink_counter==0) {
						restore_counter = RESTORE_CHECK_KEY_MS;
						system_state = state_after_blink_kernal;
						function_counter = 0;
					} else {
						// switch on
						io_port_b.bit.led1 = 0;
						function_counter = 1;
					}
					break;
				}
			if (system_state==BLINK_KERNAL)
				function_counter++;
			break;
		case CHECK_KEYS:
			if (pa0_counter==0) {
				if (function_counter < IO_DEBOUNCE_COUNT) {
					// debounce
					PIN_ChSet(PIN_PA0_CTRL,1); // Pull PortA low for own keyscan
					SYSTIME_WaitUs(3);
					(void)MCP23S18_StartRead(MCP23S18_GPIOA);
					MCP23S18_WaitForCompletion();
					PIN_ChSet(PIN_PA0_CTRL,0);
					cia_port_b[function_counter] = MCP23S18_GetRxByte();
					function_counter++;
				} else {
					// check if all reads are the same
					u8 pb = cia_port_b[0];
					u8 equal = 1;
					for (int ctr=1; ctr<IO_DEBOUNCE_COUNT; ctr++) {
						if (pb != cia_port_b[ctr]) {
							equal = 0;
							break;
						}
					}
					function_counter = 0;
					if (equal == 0) {
						// not equal - continue scanning
						pa0_counter = 20; // minimum distance in case there is no scanning on PortA0
					} else {
						pb = ~pb; // invert to see keys pressed as 1
						if ( (pb & KEY_DEL) == KEY_DEL) {
							// joystick swap
							config.io_port_b.bit.joyswap = !config.io_port_b.bit.joyswap;
							system_state = BLINK_JOYSWAP;
						} else if ( (pb & KEY_RETURN) == KEY_RETURN) {
							state_after_blink_kernal = BLINK_SID;
							state_after_blink_sid = CHECK_KEYS;
							system_state = BLINK_KERNAL;
						} else if ( (pb & KEY_RIGHT) == KEY_RIGHT) {
							config.io_port_b.bit.sid_a5_sel = !config.io_port_b.bit.sid_a5_sel;
							system_state = BLINK_SID;
							state_after_blink_sid = CHECK_KEYS;
						} else if ( (pb & KEY_DOWN) == KEY_DOWN) {
							config.io_port_b.bit.sid_a8_sel = !config.io_port_b.bit.sid_a8_sel;
							system_state = BLINK_SID;
							state_after_blink_sid = CHECK_KEYS;
						} else if ( (pb & KEY_F1) == KEY_F1) {
							// F1 either selects 0 (0b000) or 4 (0b100)
							if ((config.io_port_b.bit.kernal_a14==0) && (config.io_port_b.bit.kernal_a13==0))
								config.io_port_b.bit.kernal_a15 = !config.io_port_b.bit.kernal_a15;
							else
								config.io_port_b.bit.kernal_a15 = 0;
							config.io_port_b.bit.kernal_a14 = 0;
							config.io_port_b.bit.kernal_a13 = 0;
							system_state = BLINK_KERNAL;
							state_after_blink_kernal = CHECK_KEYS;
						} else if ( (pb & KEY_F3) == KEY_F3) {
							// F3 either selects 1 (0b001) or 5 (0b101)
							if ((config.io_port_b.bit.kernal_a14==0) && (config.io_port_b.bit.kernal_a13==1))
								config.io_port_b.bit.kernal_a15 = !config.io_port_b.bit.kernal_a15;
							else
								config.io_port_b.bit.kernal_a15 = 0;
							config.io_port_b.bit.kernal_a14 = 0;
							config.io_port_b.bit.kernal_a13 = 1;
							system_state = BLINK_KERNAL;
							state_after_blink_kernal = CHECK_KEYS;
						} else if ( (pb & KEY_F5) == KEY_F5) {
							// F3 either selects 2 (0b010) or 6 (0b110)^
							if ((config.io_port_b.bit.kernal_a14==1) && (config.io_port_b.bit.kernal_a13==0))
								config.io_port_b.bit.kernal_a15 = !config.io_port_b.bit.kernal_a15;
							else
								config.io_port_b.bit.kernal_a15 = 0;
							config.io_port_b.bit.kernal_a14 = 1;
							config.io_port_b.bit.kernal_a13 = 0;
							system_state = BLINK_KERNAL;
							state_after_blink_kernal = CHECK_KEYS;
						} else if ( (pb & KEY_F7) == KEY_F7) {
							// F3 either selects 3 (0b011) or 7 (0b111)
							if ((config.io_port_b.bit.kernal_a14==1) && (config.io_port_b.bit.kernal_a13==1))
								config.io_port_b.bit.kernal_a15 = !config.io_port_b.bit.kernal_a15;
							else
								config.io_port_b.bit.kernal_a15 = 0;
							config.io_port_b.bit.kernal_a14 = 1;
							config.io_port_b.bit.kernal_a13 = 1;
							system_state = BLINK_KERNAL;
							state_after_blink_kernal = CHECK_KEYS;
						}
					}
				}
			}
			break;
		default:
			// nop
			break;
	}

	set_ext_pins();
}

/** 10ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_10ms(u32 dummy) {
	(void)dummy;
}

/** 100ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_100ms(u32 dummy) {
	(void)dummy;
}

/** 1000ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_1000ms(u32 dummy) {
	(void)dummy;
	s_counter++;
	//io_port_b.bit.led1 = !io_port_b.bit.led1;
	//io_port_b.bit.led2 = !io_port_b.bit.led2;
	//io_port_b.bit.led1 = s_counter&1;
	//io_port_b.bit.led2 = (s_counter&2)>>1;
	//MCP23S18_Write(MCP23S18_GPIOB, io_port_b.byte); // set PortA outputs high (inactive)
}

void SysTick_Handler(void) {
	ms_counter++;
	PrioWrap_Function(task_1ms, 0);
	if (ms_counter % 10 == 0)
		PrioWrap_Function(task_10ms, 0);
	if (ms_counter % 100 == 0)
		PrioWrap_Function(task_100ms, 0);
	if (ms_counter % 1000 == 0)
		PrioWrap_Function(task_1000ms, 0);
}


void PA0_Edge_Handler(void) {
	pa0_edge_counter++;
	// if PA0 toggles faster than the 1ms interrupt can count it down, the pa0_counter could never reach 0 -> stuck
	// To avoid this, set the interrupt is locked if pa0_counter didn't reach 0 PIN_IRQ_STUCK_COUNT times.
	if (pa0_counter != 0) {
		if (++pa0_irq_stuck_counter >= PIN_IRQ_STUCK_COUNT)
			PIN_EnableRisingEdgeInt(PIN_INT(INT_PA0),0);
	} else
		pa0_irq_stuck_counter=0;
	pa0_counter = PA0_IRQ_DELAY;
	PIN_ClrIntStatus(PIN_INT(INT_PA0));
}

int main(void) {
	//u8 ctr1,ctr2;

	SystemInit();
	SYSTIME_Init();
	DMA_Init();
	DMA_IRQEnable(1);
	SPI_Init();
	PIN_Init();
	// disable switch matrix and IO config to save power
	SYSCON_DisableClockControl(SYS_CLK_SWM|SYS_CLK_IOCON);

	// wake up I/O Expander
	PIN_ChSet(PIN_IO_RESET, 1);

	// init NVM and try to read config. If it fails, the default is kept
	NVM_Init();
#if DEBUG_NVRAM
	config_nvram.io_port_b.byte = config.io_port_b.byte;
#endif
	(void)read_config(&config);
	config_old.io_port_b.byte = config.io_port_b.byte;

	MCP23S18_Write(MCP23S18_IOCON, MCP23S18_IOCON_BANK_0|MCP23S18_IOCON_MIRROR_OFF|MCP23S18_IOCON_SEQOP_EN|
			MCP23S18_IOCON_ODR_DIS|MCP23S18_IOCON_INTPOL_HI|MCP23S18_IOCON_INTCC_GPIO); // default config

	io_port_b.byte =  config.io_port_b.byte & IO_PORT_B_NOLED_MASK;
	set_leds_idle();
	// set PortA outputs
	set_ext_pins();
	MCP23S18_Write(MCP23S18_IODIRB, 0);  // configure ports as outputs
#if DEBUG_PU
	// for debugging, enable all pullups on the IO expander
	MCP23S18_Write(MCP23S18_GPPUB, 0xff);
	MCP23S18_Write(MCP23S18_GPPUA, 0xff);
#else
	// normally, only enable the pullups on the LED control lines
	MCP23S18_Write(MCP23S18_GPPUB, IO_LED1|IO_LED2);
#endif

	// Enable PortA0 interrupt
	PIN_InterruptSelect(INT_PA0, PIN_PA0);
	PRC_Locked_Bitmask((void*)&PIN_GetInterruptMode(), PIN_INT(INT_PA0), PIN_ISEL_EDGE(INT_PA0));
	PIN_EnableRisingEdgeInt(PIN_INT(INT_PA0),1);
	PIN_ClrIntStatus(PIN_INT(INT_PA0));
	NVIC_SetPriority(IRQ_PA0, PIN_IRQ);
	NVIC_EnableIRQ(IRQ_PA0);

	system_state = RESET;
	function_counter = 0;

	// Init timebase
	PrioWrap_Init();
	// SysTick and PendSV are system exceptions - they don't need to be enabled
	NVIC_SetPriority(SysTick_IRQn, SYSTICK_IRQ_PRIO);   /* set to medium prio */
	SYSTICK_Init();                                     /* 1ms system tick */
	SYSTICK_Enable(1);
	SYSTICK_EnableIRQ(1);

	while(1) {
		/* Sleep until next IRQ happens */
		__WFI();
	}
	return 0 ;
}


