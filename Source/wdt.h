#ifndef WDT_H
#define WDT_H

/** WDT library
	Watchdog Timer
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include <prc.h>

/* Watchdog mode register MOD */

#define WDT_MOD_WDEN       (1UL<<0)  ///! Watchdog enable bit (once written with 1, can't be reset to 0)
#define WDT_MOD_WDRESET    (1UL<<1)  ///! Watchdog reset enable bit (once written with 1, can't be reset to 0)
#define WDT_MOD_WDTOF      (1UL<<2)  ///! Watchdog time-out flag. Cleared by SW
#define WDT_MOD_WDINT      (1UL<<3)  ///! Warning interrupt flag. Cleared by SW
#define WDT_MOD_WDPROTECT  (1UL<<4)  ///! Watchdog updated mode: 0: can be changed any time, 1: can be changed below WDWARNINT/WDWINDOW */
#define WDT_MOD_LOCK       (1UL<<5)  ///! Prevent disabling or powering down the watchdog. Only cleared by reset. */

/** Get WDT mode register.
	@return MOD register
*/
#define WDT_GetMode() (LPC_WWDT->MOD)

/** Set WDT mode register.
	@param y value to write to MOD register
*/
#define WDT_SetMode(y)  {LPC_WWDT->MOD = (u32)(y);}
	
	
/* WDT timer constant register TC */	

/** Get WDT timer constant register.
	@return TC register
*/
#define WDT_GetCount() ((LPC_WWDT->TC)&0xffffff)

/** Set WDT timer constant.
	@param y value to write to TC register
*/
#define WDT_SetCount(y)  {LPC_WWDT->TC = (u32)(y)&0xffffff;}


/* WDT feed register FEED */
#define WDT_Service() { PRC_Int_Disable(); LPC_WWDT->TC=0xaa; LPC_WWDT->TC=0x55; PRC_Int_Enable(); }


/* WDT timer value register TV */	

/** Get WDT timer value register.
	@return TV register
*/
#define WDT_GetValue() ((LPC_WWDT->TV)&0xffffff)


/*  Watchdog Timer Warning Interrupt register WARNINT */

/** Get WDT timer warning interrupt register.
	@return WARNINT register
*/
#define WDT_GetIntCount() ((LPC_WWDT->WARNINT)&0xffffff)

/** Set WDT timer warning interrupt register.
	@param y value to write to WARNINT register
*/
#define WDT_SetIntCount(y)  {LPC_WWDT->WARNINT = (u32)(y)&0xffffff;}


/*  Watchdog Timer Warning Interrupt register WARNINT */

/** Get WDT timer window register.
	@return WINDOW register
*/
#define WDT_GetWindow() ((LPC_WWDT->WINDOW)&0xffffff)

/** Set WDT timer window register.
	@param y value to write to WINDOW register
*/
#define WDT_SetWindow(y)  {LPC_WWDT->WINDOW = (u32)(y)&0xffffff;}

#endif