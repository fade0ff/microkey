/** ADC Library
	Definitions for calling IAP ROM functions
	LPC824 ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef ADC_CNF_H
#define ADC_CNF_H


#define ADC_SAMPLE_FREQ    120000 // 480000
#define ADC_LOW_POWER_MODE 1
#define ADC_VRANGE         ADC_TRIM_VRANGE_HIGH

/* ADC channel mapping */
#define ADC_CH_V1 11
#define ADC_CH_V2  3
#define ADC_CH_V3  9
#define ADC_CH_V4 10
#define ADC_CH_V5  2

#define ADC_CHANNELS (1UL<<ADC_CH_V1)|(1UL<<ADC_CH_V2)|(1UL<<ADC_CH_V3)|(1UL<<ADC_CH_V4)|(1UL<<ADC_CH_V5)

#endif
