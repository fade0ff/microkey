/** Synchronous Serial Library - Configuration
	Communication via the SSP peripheral (i.e. SPI)
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef SPI_CNF_H
#define SPI_CNF_H

#define SPI_PHYS_DEVICE_NUM      2  ///! number of physical SSP devices

#define SPI_USE_DMA              1  ///! define whether to use a DMA based SPI transfer

/* These values have to fit the according configuration structures */
#define SPI_VIRTUAL_DEVICE_NUM   1  ///! the number of actually used devices as configured in ssp_device_ptr
#define SPI_CHANNEL_CONFIG_NUM   1  ///! number of SSP configuration channels

#define SPI_QUEUE_SIZE          16  ///! number of entries in queue per device (power of two to optimize)

/** channel access macros */
#define SPI_CH_MCP23S18          0  ///! SPI channel for Io Expander

#endif
