Microcontroller board for my 250466+ project that allows (hard) resets as well as switching Kernals, SID2 addresses and Joysticks ports through the C64 keyboard..
See http://www.lemmini.de/MicroKey/MicroKey.html

---------------------------------------------------------------
Copyright 2021 Volker Oth - VolkerOth(at)gmx.de

Licensed under the Creative Commons Attribution 4.0 license
http://creativecommons.org/licenses/by/4.0/

Everything in this repository is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OF ANY KIND, either express or implied.